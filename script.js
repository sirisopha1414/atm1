$(document).ready(function() {
	var account = {
		'1234' : {'pin': '1234', 'balance': 500},
		'5678' : {'pin': '5678', 'balance': 1500},
		'0000' : {'pin': '0000', 'balance': 2500}
	};
	
	$('.form-menu').hide();
	
	$('.form-signin').submit(function() {
		var accountNo = $('#accountNo').val();
		var pin = $('#pin').val();
		if (accountNo != null && account[accountNo]['pin'] == pin) {
			$('.form-menu').show();
			$('.form-signin').hide();
      $('.displaybalance').show();
			showbalance(accountNo);
      $('#p1').hide();
      $('#p2').hide();
      $('#deposit').click(function(){
        
				var num = parseInt($('#a1').val());
				if (isNaN(num)|| num <= 0){
					alert("จำนวนเงินไม่ถูกต้อง !");
				} else {
					deposit(accountNo, num);
				}
        
			
			});

			$('#withdraw').click(function(){
				var num = parseInt($('#a2').val());
				if (isNaN(num)|| num <= 0){
					alert("จำนวนเงินไม่ถูกต้อง !");
				} else {
					if (num <= account[accountNo]['balance']){
						withdraw(accountNo, num);
					}else{
						alert("จำนวนเงินในการถอนไม่เพียงพอ !");
					}
				}
			});

			

		} else {
			alert('เลขที่บัญชีหรือรหัสผ่านไม่ถูกต้อง');
		}
		return false;
	});

	$('#dps').click(function(){
    $('#torn').toggle();
    $('#p1').toggle();
  });
  $('#torn').click(function(){
    $('#dps').toggle();
    $('#p2').toggle();
  });
	
	function showbalance(accountNo) {
		$("#user").html('เลขที่บัญชี : ' + accountNo);
		$("#balance").html( account[accountNo]['balance'].toString() + ' บาท');
	}

	function deposit(accountNo, amount) {
		account[accountNo]['balance'] += amount;
		showbalance(accountNo);
    $('#torn').show();
    $('#p1').hide();
		alert('ทำรายการฝากเงินเรียบร้อยแล้ว');
	}

	function withdraw(accountNo, amount) {
		account[accountNo]['balance'] -= amount;
		showbalance(accountNo);
    $('#dps').show();
    $('#p2').hide();
		alert('ทำรายการถอนเงินเรียบร้อยแล้ว');
	}
});